package othetoe;
import java.awt.*;
import javax.swing.*;

public class ImagePanel extends JPanel{
   Image picture;
   ImagePanel(String link){
      ImageIcon icn = new ImageIcon(link);
      picture = icn.getImage();
   }
   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.drawImage(picture, 0, 0, null);
   }
}
