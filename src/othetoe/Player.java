package othetoe;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public enum Player {
   PLAYER1("player1", new ImageIcon("res/board/be.png") ), PLAYER2("player2",new ImageIcon("red/board/re.png"));

   PowerUp[] powerUps = new PowerUp[4];

   String name;
   int point = 0;
   ImageIcon avatar;

   Player(String name, ImageIcon avatar) {
      this.name = name;
      this.avatar = avatar;
      givePower(1,1,1,1);
   }

   void givePower(int a, int b, int c, int d){
      powerUps[0] = new PowerUp(a);
      powerUps[1] = new PowerUp(b);
      powerUps[2] = new PowerUp(c);
      powerUps[3] = new PowerUp(d);
   }
}
