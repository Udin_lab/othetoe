package othetoe;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class Board extends JFrame implements ActionListener {

   int move = 0;
   int cellCount;
   int markCount;
   // int cellSize = 500/n;
   Cell [][] cells;
   boolean justStarted = true;

   JPanel headPanel;
   JPanel bottomPanel;
   JPanel bluePanel;
   JPanel redPanel;
   JPanel startPanel;
   JPanel gamePanel;

   JPanel bluePoint;
   JLabel bluePointBg;
   JLabel bluePointCount;

   JPanel redPoint;
   JLabel redPointBg;
   JLabel redPointCount;

   JRadioButton [] board = new JRadioButton[5];
   JRadioButton [] marks = new JRadioButton[5];

   JLabel p1Img;
   JLabel p2Img;
   JRadioButton [] p1Avatars = new JRadioButton[8];
   JRadioButton [] p2Avatars = new JRadioButton[8];
   JTextField p1Name;
   JTextField p2Name;

   JPanel blueAvatarList;
   JPanel redAvatarList;

   int [] boardOptions = {5,10,15,20,25};
   int [] marksOptions = {4,5,6,7,8};

   JButton submitGameSetup;

   JTextField pointToWin;

   Board(){
      this.setLayout(null);
      this.setSize(new Dimension(1000,710));
      this.getContentPane().setBackground(new Color(40,40,40));
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      this.setResizable(false);

      bluePaneSetup();
      redPanelSetup();
      headSetup();
      bottomSetup();
      startPanelSetup();
      
      // this.add(gamePanel);
      this.add(startPanel);
      this.add(bluePanel);
      this.add(redPanel);
      this.add(headPanel);
      this.add(bottomPanel);
      
      this.setVisible(true);
   }

   void gamePanelSetup(){
      gamePanel = new JPanel();
      gamePanel.setBackground(Color.blue);
      gamePanel.setBounds(250,110,500,500); 
      gamePanel.setLayout(new GridLayout(cellCount,cellCount,0,0));
      cells = new Cell[cellCount][cellCount];
      for (int i = 0; i<cellCount; ++i){
         for (int j = 0; j<cellCount; ++j){
            cells[i][j] = new Cell (500/cellCount,i,j);
            cells[i][j].addActionListener(this);
            // cells[i][j].setLayout(new GridLayout(1,1,1,1));
            gamePanel.add(cells[i][j]);
         }
      }
      // gamePanel.setLayout( new GridLayout(n,n,0,0) );

   }

   void startPanelSetup(){
      startPanel = new JPanel();
      startPanel.setBounds(250, 200, 500, 300);
      startPanel.setBackground(Color.CYAN);
      startPanel.setLayout(new FlowLayout());

      //title
      JLabel startTitle = new JLabel("Game Setup");
      startTitle.setPreferredSize(new Dimension(450,80));
      startPanel.add(startTitle);

      //board n x n
      
      board[0] = new JRadioButton("5 x 5");
      board[0].setSelected(true);
      board[1] = new JRadioButton("10 x 10");
      board[2] = new JRadioButton("15 x 15");
      board[3] = new JRadioButton("20 x 20");
      board[4] = new JRadioButton("25 x 25");

      ButtonGroup boardGroup= new ButtonGroup();

      for (int i = 0; i<5; i++){
         board[i].setFocusable(false);
         board[i].setPreferredSize(new Dimension(80,30));
         boardGroup.add(board[i]);
      }

      //marks a row to win
      
      marks[0] = new JRadioButton("4");
      marks[0].setSelected(true);
      marks[1] = new JRadioButton("5");
      marks[2] = new JRadioButton("6");
      marks[3] = new JRadioButton("7");
      marks[4] = new JRadioButton("8");

      ButtonGroup marksGroup= new ButtonGroup();

      for (int i = 0; i<5; i++){
         marks[i].setPreferredSize(new Dimension(80,30));
         marks[i].setFocusable(false);
         marksGroup.add(marks[i]);
      }

      JLabel boardLabel = new JLabel("Board : ");
      startPanel.add(boardLabel);
      for (int i = 0 ; i<5; i++){
         startPanel.add(board[i]);
      }
      JLabel marksLabel = new JLabel("Marks : ");
      startPanel.add(marksLabel);
      for (int i = 0 ; i<5; i++){
         startPanel.add(marks[i]);
      }

      //submit button
      submitGameSetup = new JButton("GO!");
      submitGameSetup.setPreferredSize(new Dimension(100,40));
      submitGameSetup.addActionListener(this);

      JLabel pointLabel = new JLabel("Point To Win : ");
      startPanel.add(pointLabel);

      pointToWin = new JTextField();
      pointToWin.setPreferredSize(new Dimension (100,40));
      startPanel.add(pointToWin);
      
      JLabel voidLabel = new JLabel();
      voidLabel.setPreferredSize(new Dimension(450,1));

      startPanel.add(voidLabel);
      startPanel.add(submitGameSetup);
   }

   void bluePaneSetup(){
      bluePanel = new JPanel();
      bluePanel.setBackground(new Color(0,0,0,0));
      bluePanel.setBounds(10, 100, 230, 510);
      bluePanel.setLayout(null);
      
      JLabel blueBg = new JLabel();
      blueBg.setIcon(new ImageIcon("res/board/pb.png"));
      blueBg.setBounds(0, 0, 229, 510);
      
      p1Img = new JLabel();
      p1Img.setIcon(new ImageIcon("res/board/be.png"));
      p1Img.setBounds(27,27,175,175);

      blueAvatarList = new JPanel();
      blueAvatarList.setLayout(new FlowLayout());
      blueAvatarList.setBounds(27, 205, 175, 90 );
      blueAvatarList.setBackground(new Color(255,255,255,100));
      ButtonGroup blueAvatarGroup = new ButtonGroup();      

      //blue
      bluePoint= new JPanel();
      bluePoint.setBounds(60,250,110,110);
      bluePoint.setLayout(null);
      bluePoint.setBackground(new Color(0,0,0,0));

      bluePointBg= new JLabel();
      bluePointBg.setIcon(new ImageIcon("res/board/bpoint.png"));
      bluePointBg.setBounds(0,0,110,110);

      bluePointCount = new JLabel();
      bluePointCount.setBounds(0,0,110,110);
      bluePointCount.setBackground(Color.red);
      bluePointCount.setText(""+Player.PLAYER1.point);
      bluePointCount.setFont( new Font("Minecraft", Font.PLAIN, 30));
      bluePointCount.setVerticalAlignment(JLabel.CENTER);
      bluePointCount.setHorizontalAlignment(JLabel.CENTER );
      
      bluePoint.add(bluePointCount);
      bluePoint.add(bluePointBg);
      bluePoint.setVisible(false);
      
      for (int i = 1; i<=8; i++){
         p1Avatars[i-1] = new JRadioButton();
         Image img = new ImageIcon("res/avatar/"+i+".png").getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH);
         p1Avatars[i-1].setIcon(new ImageIcon( img ));
         p1Avatars[i-1].setBackground(new Color(0,0,0,0));
         p1Avatars[i-1].addActionListener(this);
         blueAvatarGroup.add(p1Avatars[i-1]);
         blueAvatarList.add(p1Avatars[i-1]);
      }

      bluePanel.add(bluePoint);
      bluePanel.add(blueAvatarList);
      bluePanel.add(p1Img);
      bluePanel.add(blueBg);
   }

   void redPanelSetup(){
      redPanel = new JPanel();
      redPanel.setBackground(new Color(0,0,0,0));
      redPanel.setBounds(761, 100, 230, 510);
      redPanel.setLayout(null);
      
      JLabel redBg = new JLabel();
      redBg.setIcon(new ImageIcon("res/board/pr.png"));
      redBg.setBounds(0, 0, 229, 510);

      p2Img = new JLabel();
      p2Img.setIcon(new ImageIcon("res/board/re.png"));
      p2Img.setBounds(27,27,175,175);
      
      redAvatarList = new JPanel();
      redAvatarList.setLayout(new FlowLayout());
      redAvatarList.setBounds(27, 205, 175, 90 );
      redAvatarList.setBackground(new Color(255,255,255,100));
      ButtonGroup redAvatarGroup = new ButtonGroup();

      //blue
      redPoint= new JPanel();
      redPoint.setBounds(60,250,110,110);
      redPoint.setLayout(null);
      redPoint.setBackground(new Color(0,0,0,0));

      redPointBg= new JLabel();
      redPointBg.setIcon(new ImageIcon("res/board/rpoint.png"));
      redPointBg.setBounds(0,0,110,110);

      redPointCount = new JLabel();
      redPointCount.setBounds(0,0,110,110);
      redPointCount.setBackground(Color.red);
      redPointCount.setText(""+Player.PLAYER2.point);
      redPointCount.setFont( new Font("Minecraft", Font.PLAIN, 30));
      redPointCount.setVerticalAlignment(JLabel.CENTER);
      redPointCount.setHorizontalAlignment(JLabel.CENTER );
      
      redPoint.add(redPointCount);
      redPoint.add(redPointBg);
      redPoint.setVisible(false);
      
      for (int i = 1; i<=8; i++){
         p2Avatars[i-1] = new JRadioButton();
         Image img = new ImageIcon("res/avatar/"+(i+8)+".png").getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH);
         p2Avatars[i-1].setIcon(new ImageIcon( img ));
         p2Avatars[i-1].setBackground(new Color(0,0,0,0));
         p2Avatars[i-1].addActionListener(this);
         redAvatarGroup.add(p2Avatars[i-1]);
         redAvatarList.add(p2Avatars[i-1]);
      }
      
      redPanel.add(redPoint);
      redPanel.add(redAvatarList);
      redPanel.add(p2Img);
      redPanel.add(redBg);
   }


   void headSetup(){
      headPanel = new JPanel();
      headPanel.setBounds(47, 0, 906, 97);
      headPanel.setBackground(new Color(0,0,0,0));
      
      JLabel head = new JLabel();
      head.setIcon(new ImageIcon("res/board/head.png"));

      headPanel.add(head);
   }

   void bottomSetup(){
      bottomPanel = new JPanel();
      bottomPanel.setBounds(47,615, 906,57 );
      bottomPanel.setBackground(new Color(0,0,0,0));

      JLabel bottom = new JLabel();
      bottom.setIcon(new ImageIcon( "res/board/ra.png" ));

      bottomPanel.add(bottom);
   }

   boolean isDiagonalLeft(int x, int y) {
		int count  = 1;
		Cell core = cells[x][y];	
		
		for (int i = 1; i<cellCount; i++) {
			if ( x-i >= 0 && y-i>=0 ) {
				if (cells[x-i][y-i].player == core.player) {
					count++;
				}
				else {
					break;
				}
         }
         else{
            break;
         }
		}
		
		for (int i = 1; i<cellCount; i++) {
			if ( x+i < cellCount && y+i<cellCount) {
				if (cells[x+i][y+i].player == core.player) {
					count++;
				}
				else {
					break;
				}
         }
         else {
            break;
         }
		}
//		System.out.println(count +" "+ core.player.playerName);
      return count>=markCount;
   }

   boolean isDiagonalRight(int x, int y) {
		int count  = 1;
		Cell core = cells[x][y];	
		
		for (int i = 1; i<cellCount; i++) {
			if ( x-i >= 0 && y+i<cellCount ) {
				if (cells[x-i][y+i].player == core.player) {
					count++;
				}
				else {
					break;
				}
         }
         else{
            break;
         }
		}
		
		for (int i = 1; i<cellCount; i++) {
			if ( x+i <cellCount && y-i>=0) {
				if (cells[x+i][y-i].player == core.player) {
					count++;
				}
				else {
					break;
				}
         }
         else{
            break;
         }
		}
//		System.out.println(count +" "+ core.player.playerName);
		return count>=markCount;
	}
	boolean isVertical(int x, int y) {
		int count  = 1;
		Cell core = cells[x][y];	
		
		for (int i = 1; i<cellCount; i++) {
			if ( x-i >= 0 ) {
				if (cells[x-i][y].player == core.player) {
					count++;
				}
				else {
					break;
				}
         }
         else{
            break;
         }
		}
		
		for (int i = 1; i<cellCount; i++) {
			if ( x+i < cellCount){
				if (cells[x+i][y].player == core.player) {
					count++;
				}
				else {
					break;
				}
         }
         else{
            break;
         }
		};
//		System.out.println(count +" "+ core.player.playerName);
		return count>=markCount;
	}
	boolean isHorizontal(int x, int y) {
		int count  = 1;
		Cell core = cells[x][y];	
		
		for (int i = 1; i<cellCount; i++) {
			if (y+i<cellCount ) {
				if (cells[x][y+i].player == core.player) {
					count++;
				}
				else {
					break;
				}
         }
         else{
            break;
         }
		}
		
		for (int i = 1; i<cellCount; i++) {
			if ( y-i>=0) { 
				if (cells[x][y-i].player == core.player) {
					count++;
				}
				else {
					break;
				}
         }
         else{
            break;
         }
		}
//		System.out.println(count +" "+ core.player.playerName);
		return count>=markCount;
	}

   @Override
   public void actionPerformed(ActionEvent e) {
      // TODO Auto-generated method stub
      for (int i = 0; i<8; i++){
         if (e.getSource() == p1Avatars[i]){
            Image img = new ImageIcon("res/avatar/"+(i+1)+".png").getImage().getScaledInstance(173, 173, Image.SCALE_SMOOTH);
            p1Img.setIcon(new ImageIcon(img));
         }
      }

      for (int i = 0; i<8; i++){
         if (e.getSource() == p2Avatars[i]){
            Image img = new ImageIcon("res/avatar/"+(i+1+8)+".png").getImage().getScaledInstance(173, 173, Image.SCALE_SMOOTH);
            p2Img.setIcon(new ImageIcon(img));
         }
      }

      if (e.getSource() == submitGameSetup ){
         // System.out.println("go!");
         for (int i = 0; i<5; i++){
            if (board[i].isSelected()){
               cellCount = boardOptions[i];
            }
            if (marks[i].isSelected()){
               markCount = marksOptions[i];
            }
         }

         if(JOptionPane.showConfirmDialog(null, "Lanjutkan Ke Permainan?", "" , JOptionPane.YES_NO_OPTION) == 0){
            bluePanel.remove(blueAvatarList);
            redPanel.remove(redAvatarList);
            gamePanelSetup();
            this.add(gamePanel);
            this.remove(startPanel);
            
            redPoint.setVisible(true);
            bluePoint.setVisible(true);

            this.revalidate();
            this.repaint();

         }

      }

      for (int i = 0; i<cellCount; ++i){
         for (int j = 0; j<cellCount; ++j){
            Cell focus = cells[i][j];
            if (e.getSource() == cells[i][j]){

               if (move%2 == 0){
                  if (focus.player == null){
                     Image img = new ImageIcon("res/board/cb1.png").getImage();
                     img = img.getScaledInstance( 500/cellCount-2, 500/cellCount-2, Image.SCALE_SMOOTH);
                     focus.setIcon(new ImageIcon(img));
                     focus.setBackground(new Color(80,80,80));
                     focus.player= Player.PLAYER1;
                     move++;
                  }
               }

               else {
                  if (focus.player == null){
                     
                     Image img = new ImageIcon("res/board/cr1.png").getImage();
                     img = img.getScaledInstance( 500/cellCount-2, 500/cellCount-2, Image.SCALE_SMOOTH);
                     focus.setIcon(new ImageIcon(img));
                     focus.setBackground(new Color(80,80,80));
                     focus.player= Player.PLAYER2;
                     move++;
                  }
               }

               System.out.println("left " + isDiagonalLeft(i,j));
					System.out.println("right " + isDiagonalRight(i,j));
					System.out.println("vertical " + isVertical(i, j));
					System.out.println("horizontal " + isHorizontal(i, j));
            }
         }
      }
   }
}
